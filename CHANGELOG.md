## 1.0.1+2
* Fix: CubitState don't need close Cubit manually

## 1.0.1+1
* Fix: Remove global **bloc variable** from CubitState

## 1.0.1
* Allow CubitState access StatefulWidget using **currentWidget**
* Add common Fastfile lanes

## 1.0.0
BREAKING CHANGE:
* Remove LoadingService

## 0.0.2+3
* Hide components from GetConnect of GetX package

## 0.0.2+2
* Fix PhoneAuthService is not completed when dismiss OTP field
* Mark LoadingService methods deprecated
* showErrorMessage on GenericBaseCubit use SnackBar instead Dialog

## 0.0.2+1
* Handle exception when PhoneAuthService isn't config correctly
* Remove LoadingService on PhoneAuthService

## 0.0.2
* Add MediaService to pick image from Camera and Gallery
* Add static logger
* Add PhoneNumber properties: e164, isValidPhoneNumber, callingCode, parsedNumber
* Publish DisposableWidget
* Improve Notification permission asking
* Clean code, update some styling
* Update packages 

## 0.0.1
* Initial release

