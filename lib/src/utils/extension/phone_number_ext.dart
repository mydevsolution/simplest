import 'package:libphonenumber/libphonenumber.dart';
import 'package:simplest/simplest.dart';

extension PhoneNumberExt on PhoneNumber {
  Future<bool> get isValidPhoneNumber async {
    try {
      final phoneWithRegion =
          await PhoneNumber.getRegionInfoFromPhoneNumber(phoneNumber, isoCode);
      return PhoneNumberUtil.isValidPhoneNumber(
          phoneNumber: phoneWithRegion.phoneNumber,
          isoCode: phoneWithRegion.isoCode);
    } catch (error) {
      return false;
    }
  }

  String get e164 => '+$callingCode$parsedNumber';
  String get callingCode => dialCode.replaceAll('+', '');
  String get parsedNumber => parseNumber().replaceAll('+', '');
}
