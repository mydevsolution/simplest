import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void hideStatusBar() {
  SystemChrome.setEnabledSystemUIOverlays([]);
}

void showStatusBar() {
  SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
}

void hideKeyboard(BuildContext context, {FocusNode focusNode}) {
  if (focusNode != null) {
    if (focusNode.hasPrimaryFocus) {
      focusNode.unfocus();
    } else {
      FocusScope.of(context).unfocus();
    }
  } else {
    FocusScope.of(context).unfocus();
  }
}
