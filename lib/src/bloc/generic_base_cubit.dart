import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:simplest/simplest.dart';
import 'package:simplest/src/services/abstract/app_config.dart';
import 'package:stacked_services/stacked_services.dart';

final getIt = GetIt.instance;

abstract class GenericBaseCubit<State> extends Cubit<State> {
  GenericBaseCubit(State state) : super(state) {
    initData();
  }

  NavigationService get navigator => getIt<NavigationService>();
  DialogService get dialogService => getIt<DialogService>();
  SnackbarService get snackbarService => getIt<SnackbarService>();
  NetworkActivityService get networkActivity => getIt<NetworkActivityService>();
  IAppConfig get appConfig => getIt<IAppConfig>();

  List<StreamSubscription> subscriptions = [];

  /// Called when init Cubit
  void initData() {}

  void showErrorMessage(String error) {
    snackbarService.showSnackbar(message: error.tr);
  }

  Future<DialogResponse> showDialog(
      {String title,
      String description,
      String cancelTitle,
      String buttonTitle = 'ok',
      bool barrierDismissible = false}) {
    return dialogService.showDialog(
        title: title ?? appConfig.appName,
        description: description,
        cancelTitle: cancelTitle,
        buttonTitle: buttonTitle.tr,
        barrierDismissible: barrierDismissible);
  }

  @override
  Future<void> close() {
    subscriptions.forEach((sub) {
      sub.cancel();
    });
    return super.close();
  }
}
