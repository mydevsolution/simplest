const keys = {
  'account_exists_with_different_credential':
      'Account exists with different credential',
  'invalid_credential': 'Invalid credential',
  'operation_not_allowed': 'Operation not allowed',
  'user_disabled': 'User disabled',
  'user_not_found': 'User not found',
  'wrong_password': 'Wrong password',
  'app_not_authorized': 'App not authorized',
  'unable_to_verify_phone_number': 'Unable to verify phone number',
  'invalid_verification_code': 'Invalid verification code',
  'loading': 'Loading...',
  'error': 'Error',
  'please_enable_location': 'Please enable location services',
  'ok': 'OK',
  'cancel': 'Cancel',
  'camera': 'Camera',
  'gallery': 'Gallery'
};
