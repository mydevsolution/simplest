import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NetworkErrorWidget extends StatelessWidget {
  final Function onTryAgain;

  const NetworkErrorWidget({Key key, this.onTryAgain}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                Icons.cloud_off,
                size: 160,
                color: Colors.grey,
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                'network_error'.tr,
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 16,
              ),
              SizedBox(
                height: 40,
                width: double.infinity,
                child: FlatButton(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(width: 1.0, color: Colors.red),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  onPressed: onTryAgain,
                  child: Text(
                    'try_again'.tr,
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
