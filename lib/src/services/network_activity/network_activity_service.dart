import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simplest/src/utils/network_checker.dart' as network;

import 'network_error_widget.dart';

class NetworkActivityService {
  GlobalKey<NavigatorState> get _navigatorKey {
    return Get.key;
  }

  Future<bool> get hasNetwork {
    return network.hasNetwork;
  }

  Completer<void> _networkCompleter;

  Future<void> showRequireNetwork() {
    _networkCompleter = Completer<void>();
    _goToNetworkWidget();
    return _networkCompleter.future;
  }

  void _goToNetworkWidget() {
    _navigatorKey.currentState.push(
      MaterialPageRoute(
        builder: (context) {
          return NetworkErrorWidget(
            onTryAgain: () {
              _recheckNetwork();
            },
          );
        },
      ),
    );
  }

  void _recheckNetwork() async {
    if (await hasNetwork) {
      _navigatorKey.currentState.pop();
      _networkCompleter.complete();
    }
  }
}
