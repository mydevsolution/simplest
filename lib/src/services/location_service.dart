import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';

class LocationService {
  StreamSubscription<Position> _positionSubscription;
  final BehaviorSubject<Position> _positionSubject =
      BehaviorSubject<Position>();
  // final _kDefaultPosition =
  //     Position(latitude: 10.850739, longitude: 106.626114);
  final _kDefaultPosition = Position(latitude: 0, longitude: 0);

  Future<bool> fetchLocation({bool askPermission = false}) async {
    try {
      if (_positionSubscription != null) {
        await _positionSubscription.cancel();
      }

      final currentLocation = await getCurrentPosition();
      _positionSubject.add(currentLocation);
      _positionSubscription = getPositionStream().listen((position) {
        _positionSubject.add(position);
      });
      return true;
    } catch (error) {
      return _handleLocationException(error, askPermission: askPermission);
    }
  }

  Future<bool> requestPermissionAndService() async {
    // Check and request permissions
    try {
      await requestPermission();
      // Check location service is enabled or not
      bool isLocationEnabled = await isLocationServiceEnabled();
      bool isPermissionAllow = await _isAllowPermission;
      if (!isLocationEnabled || !isPermissionAllow) {
        await Get.defaultDialog(
          title: 'localtion'.tr,
          content: Text(
            'please_enable_location'.tr,
          ),
          barrierDismissible: false,
          onConfirm: () {
            openLocationSettings();
          },
          textConfirm: 'ok'.tr,
        );
        return false;
      }
      await fetchLocation();
      return true;
    } catch (error) {
      print(error);
    }
    return false;
  }

  Future<bool> _handleLocationException(dynamic error,
      {bool askPermission}) async {
    if (error is PermissionDeniedException && askPermission) {
      return requestPermissionAndService();
    }
    return false;
  }

  Future<double> distanceBetween(
      double fromLat, double fromLng, double toLat, double toLng) async {
    final double distance =
        await distanceBetween(fromLat, fromLng, toLat, toLng);
    return distance;
  }

  void close() {
    _positionSubscription.cancel();
    _positionSubject.close();
  }

  Future<bool> get _isAllowPermission async {
    final permissions = await checkPermission();
    return permissions == LocationPermission.whileInUse ||
        permissions == LocationPermission.always;
  }

  Position get position => _positionSubject.value ?? _kDefaultPosition;

  Stream<Position> get positionStream => _positionSubject.stream;
}
