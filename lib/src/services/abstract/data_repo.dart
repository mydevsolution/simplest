import 'package:dio/dio.dart';

abstract class IDataRepo {
  Dio dio;

  IDataRepo() {
    initRepo();
    reloadHeaders();
  }

  void initRepo();

  void reloadHeaders();
}
