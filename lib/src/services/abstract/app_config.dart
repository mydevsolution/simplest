abstract class IAppConfig {
  String get appName;
  String get baseUrl;
  String get env;
}
