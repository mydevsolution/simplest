import 'package:flutter/material.dart';

TextStyle heading1 = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w500,
  fontSize: 24,
);

TextStyle body1 = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.normal,
  fontSize: 16,
);

TextStyle button = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);

TextStyle heading35Black = TextStyle(
  color: Colors.black,
  fontSize: 35.0,
  fontWeight: FontWeight.bold,
);
