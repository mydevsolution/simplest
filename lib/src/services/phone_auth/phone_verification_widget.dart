import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import 'styles.dart';

class PhoneVerificationWidget extends StatefulWidget {
  final Function(String) onChange;
  final Function(String) onDone;
  final Function onResend;
  final Function(String) onSubmit;
  final int pinLength;

  PhoneVerificationWidget(
      {Key key,
      this.onChange,
      @required this.onDone,
      @required this.onResend,
      @required this.pinLength,
      @required this.onSubmit})
      : super(key: key);

  @override
  _PhoneVerificationWidgetState createState() =>
      _PhoneVerificationWidgetState();
}

class _PhoneVerificationWidgetState extends State<PhoneVerificationWidget> {
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(10.0),
              topRight: const Radius.circular(10.0))),
      child: SingleChildScrollView(
          child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          padding: EdgeInsets.fromLTRB(
            20,
            0.0,
            20,
            MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 10.0),
                  child: Text(
                    'phone_verification'.tr,
                    style: heading1,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(
                    'enter_otp_here'.tr,
                    style: body1.apply(color: Colors.grey),
                  ),
                ),
                Container(
                  child: PinCodeTextField(
                    pinBoxWidth: MediaQuery.of(context).size.width / 8,
                    isCupertino: true,
                    autofocus: true,
                    controller: _textEditingController,
                    hideCharacter: false,
                    highlight: true,
                    highlightColor: context.theme.secondaryHeaderColor,
                    defaultBorderColor: Colors.black,
                    hasTextBorderColor: context.theme.primaryColor,
                    maxLength: widget.pinLength,
                    maskCharacter: '*',
                    onTextChanged: (text) {
                      if (widget.onChange != null) {
                        widget.onChange(text);
                      }
                    },
                    onDone: (text) {
                      widget.onDone(text);
                    },
                    wrapAlignment: WrapAlignment.start,
                    pinBoxDecoration:
                        ProvidedPinBoxDecoration.underlinedPinBoxDecoration,
                    pinTextStyle: heading35Black,
                    pinTextAnimatedSwitcherTransition:
                        ProvidedPinBoxTextAnimation.scalingTransition,
                    pinTextAnimatedSwitcherDuration:
                        Duration(milliseconds: 300),
                  ),
                ),
                SizedBox(height: 24),
                ButtonTheme(
                  height: 50.0,
                  minWidth: MediaQuery.of(context).size.width - 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    elevation: 0.0,
                    color: context.theme.primaryColor,
                    child: Text(
                      'verify_now'.tr,
                      style: button,
                    ),
                    onPressed: () {
                      widget.onSubmit(_textEditingController.text);
                    },
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: () => widget.onResend(),
                          child: Text(
                            'did_not_get_otp'.tr,
                            style: TextStyle(
                              color: context.theme.primaryColor,
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    )),
              ]),
        ),
      )),
    );
  }
}
