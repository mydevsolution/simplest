import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simplest/simplest.dart';

import 'phone_verification_widget.dart';

GetIt _locator = GetIt.instance;

class PhoneAuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  Completer<PhoneAuthResponse> _verifyCompleter;
  bool _isVerificationShowing = false;

  GlobalKey<NavigatorState> get _navigatorKey {
    return Get.key;
  }

  BuildContext get context {
    return _navigatorKey.currentState.overlay.context;
  }

  int _forceResendingToken;
  String _verificationId;
  String _phoneNumber;

  /// Phone number in e164 format. Ex: +84933723831
  Future<PhoneAuthResponse> verifyPhoneNumber(String phoneNumber) async {
    try {
      await _clean();
      _verifyCompleter = Completer<PhoneAuthResponse>();
      _requestPhoneOTP(phoneNumber);
    } catch (e) {
      _verifyCompleter.completeError(e);
    }
    return _verifyCompleter.future;
  }

  void _requestPhoneOTP(String phoneNumber) async {
    try {
      _phoneNumber = phoneNumber;
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 60),
          verificationCompleted: (AuthCredential credential) async {
            _verifyAuthCredential(credential);
          },
          verificationFailed: (FirebaseAuthException authException) async {
            _verifyCompleter
                .completeError(authException.code.replaceAll('-', '_'));
            _navigatorKey.currentState.pop();
            await _clean();
          },
          codeSent: (String verificationId, int forceResendingToken) {
            _verificationId = verificationId;
            _forceResendingToken = _forceResendingToken;
            _showVerificationDialog();
          },
          forceResendingToken: _forceResendingToken,
          codeAutoRetrievalTimeout: (String verificationId) {
            _verificationId = verificationId;
          });
    } catch (e) {
      _verifyCompleter.completeError(e);
    }
  }

  void _verifyOtp(String otpCode) async {
    final AuthCredential credential = PhoneAuthProvider.credential(
      verificationId: _verificationId,
      smsCode: otpCode,
    );
    _verifyAuthCredential(credential);
  }

  void _verifyAuthCredential(AuthCredential credential) async {
    try {
      final _userCredential = await _auth.signInWithCredential(credential);
      final verifyToken = await _userCredential.user.getIdToken();
      _verifyCompleter.complete(PhoneAuthResponse(verifyToken));
      _navigatorKey.currentState.pop();
      await _clean();
    } catch (error) {
      if (error is FirebaseAuthException) {
        final _errorCode = error.code.replaceAll('-', '_');
        _locator<SnackbarService>().showSnackbar(message: _errorCode.tr);
      }
    }
  }

  void _resendOtp() {
    _requestPhoneOTP(_phoneNumber);
  }

  void _showVerificationDialog() {
    if (_isVerificationShowing) {
      _navigatorKey.currentState.pop();
    }
    _isVerificationShowing = true;
    showModalBottomSheet(
      isDismissible: false,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      isScrollControlled: true,
      context: context,
      builder: (_) {
        return PhoneVerificationWidget(
          onSubmit: (code) {
            _verifyOtp(code);
          },
          onResend: () {
            _resendOtp();
          },
          onDone: (code) {
            _verifyOtp(code);
          },
          pinLength: 6,
        );
      },
    ).whenComplete(() {
      _isVerificationShowing = false;
      if (_verifyCompleter != null && !_verifyCompleter.isCompleted) {
        _verifyCompleter.completeError('unable_to_verify_phone_number'.tr);
      }
    });
  }

  void _clean() async {
    await _auth.signOut();
    _verifyCompleter = null;
    _verificationId = null;
    _forceResendingToken = null;
    _phoneNumber = null;
  }
}

class PhoneAuthResponse {
  final String verifyToken;

  PhoneAuthResponse(this.verifyToken);
}
