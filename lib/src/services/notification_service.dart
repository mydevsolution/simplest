import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';

class NotificationService {
  String get fcmToken => tokenSubject.value ?? '';
  Stream<String> get tokenStream => tokenSubject.stream;
  Map<String, dynamic> get data => _dataSubject.value;
  Stream<Map<String, dynamic>> get dataStream => _dataSubject.stream;

  bool isShowLocalNotification = false;

  final BehaviorSubject<String> tokenSubject = BehaviorSubject<String>();
  final BehaviorSubject<Map<String, dynamic>> _dataSubject =
      BehaviorSubject<Map<String, dynamic>>();

  static Future<NotificationService> init(
      {bool isShowLocalNotification = false}) async {
    final notificationService = NotificationService();
    await notificationService.setupNotification();
    notificationService.isShowLocalNotification = isShowLocalNotification;
    return notificationService;
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final FlutterLocalNotificationsPlugin _localNotification =
      FlutterLocalNotificationsPlugin();

  static Future<dynamic> onBackgroundMessage(
      Map<String, dynamic> message) async {
    // var data = message['data'] ?? message;
    // final _payload = json.encode(data);
    // _dataSubject.add({'onMessage': _payload});
    // return;
    print(message);
    return;
  }

  void setupNotification() async {
    final fcmToken = await _firebaseMessaging.getToken();
    tokenSubject.add(fcmToken);
    _firebaseMessaging.configure(
      // onBackgroundMessage: onBackgroundMessage,
      onMessage: (Map<String, dynamic> message) async {
        if (isShowLocalNotification) {
          _displayLocalNotification(message);
        } else {
          var data = message['data'] ?? message;
          final _payload = json.encode(data);
          _dataSubject.add({'onMessage': _payload});
        }
        return;
      },
      onResume: (Map<String, dynamic> message) async {
        var data = message['data'] ?? message;
        final _payload = json.encode(data);
        _dataSubject.add({'onResume': _payload});
        return;
      },
      onLaunch: (Map<String, dynamic> message) async {
        var data = message['data'] ?? message;
        final _payload = json.encode(data);
        _dataSubject.add({'onLaunch': _payload});
        return;
      },
    );

    _setupLocalNotification();
  }

  void _setupLocalNotification() {
    final initAndroidSettings =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final initIOSSettings = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false);

    final initSettings = InitializationSettings(
        android: initAndroidSettings, iOS: initIOSSettings);

    _localNotification.initialize(initSettings,
        onSelectNotification: _onSelectLocalNotification);
  }

  // _getdata(String payload) {
  // logger.d('_getdata: $payload');
  // if (onHandleData == null) {
  //   logger.e('onHandleData not set');
  //   return;
  // }
  // onHandleData(payload);
  // }

  void _displayLocalNotification(Map<String, dynamic> message) async {
    var androidDetails = AndroidNotificationDetails(
        'fcm_default_channel', 'flutter_fcm', 'your channel description',
        importance: Importance.max, priority: Priority.high);

    var iOSDetails = IOSNotificationDetails();

    var platformChannelSpecifics =
        NotificationDetails(android: androidDetails, iOS: iOSDetails);

    if (Platform.isAndroid) {
      final _payload = json.encode(message['data']);
      _dataSubject.add({'onMessage': _payload});
      await _localNotification.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        platformChannelSpecifics,
        payload: json.encode(message['data']),
      );
    } else {
      var data = message['data'] ?? message;
      final title = data['aps']['alert']['title'];
      final body = data['aps']['alert']['body'];
      final _payload = json.encode(data);
      _dataSubject.add({'onMessage': _payload});
      await _localNotification.show(
        0,
        title,
        body,
        platformChannelSpecifics,
        payload: json.encode(data),
      );
    }
  }

  Future _onSelectLocalNotification(String payload) async {
    if (payload != null) {
      // debugPrint('notification payload: ' + payload);
      // _getdata(payload);
      _dataSubject.add({'onSelectLocalNotification': payload});
    }
  }

  void requestPermission(
      {bool sound = true, bool badge = true, bool alert = true}) {
    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions(
          IosNotificationSettings(sound: sound, badge: badge, alert: alert));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print('Settings registered: $settings');
      });
    }
  }
}
