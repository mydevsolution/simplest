import 'package:get/get.dart';

class allTranslations {
  @Deprecated('Use key.tr instead')
  static String text(String key) {
    return key.tr;
  }
}
