import 'package:example/src/screens/login/cubit/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:simplest/simplest.dart';

class LoginStatelessScreen extends CubitWidget<LoginCubit, LoginState> {
  static BlocProvider<LoginCubit> provider() {
    return BlocProvider(
      create: (context) => LoginCubit(),
      child: LoginStatelessScreen(),
    );
  }

  @override
  Widget builder(BuildContext context, LoginState state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Stateless'),
      ),
    );
  }
}
