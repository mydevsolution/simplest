import 'package:flutter/material.dart';
import 'package:simplest/simplest.dart';

import 'cubit/login_cubit.dart';

class LoginScreen extends StatefulWidget {
  static BlocProvider<LoginCubit> provider() {
    return BlocProvider(
      create: (context) => LoginCubit(),
      child: LoginScreen(),
    );
  }

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends CubitState<LoginScreen, LoginCubit, LoginState> {
  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
  }

  @override
  Widget builder(BuildContext context, LoginState state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
    );
  }
}
