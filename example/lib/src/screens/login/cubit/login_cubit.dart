import 'package:simplest/simplest.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.dart';
part 'login_cubit.freezed.dart';

class LoginCubit extends GenericBaseCubit<LoginState> {
  LoginCubit() : super(LoginState.initial());

  @override
  Future<void> close() {
    return super.close();
  }
}
