import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:simplest/simplest.dart';

part 'splash_state.dart';
part 'splash_cubit.freezed.dart';

class SplashCubit extends GenericBaseCubit<SplashState> {
  SplashCubit() : super(SplashState.initial());

  void loadData() async {
    await Future.delayed(Duration(seconds: 3));
    emit(Home());
  }
}
