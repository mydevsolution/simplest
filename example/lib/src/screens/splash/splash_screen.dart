import 'package:example/routes.dart';
import 'package:example/src/screens/splash/cubit/splash_cubit.dart';
import 'package:flutter/material.dart';
import 'package:simplest/simplest.dart';

class SplashScreen extends StatefulWidget {
  final String initialMessage = 'Initial Message';
  @override
  _State createState() => _State();

  static BlocProvider<SplashCubit> provider() {
    return BlocProvider(
      create: (context) => SplashCubit(),
      child: SplashScreen(),
    );
  }
}

class _State extends CubitState<SplashScreen, SplashCubit, SplashState> {
  @override
  void afterFirstLayout(BuildContext context) {
    context.read<SplashCubit>().loadData();
  }

  @override
  Widget builder(BuildContext context, SplashState state) {
    return Scaffold(
      body: _buildBody,
    );
  }

  @override
  void listener(BuildContext context, SplashState state) {
    if (state is Home) {
      navigator.pushNamed(AppRoute.homeScreen);
    }
  }

  Widget get _buildBody => Container(
        child: Center(child: Text('SplashScreen')),
      );
}
