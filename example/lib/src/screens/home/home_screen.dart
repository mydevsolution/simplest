import 'package:example/routes.dart';
import 'package:flutter/material.dart';
import 'package:simplest/simplest.dart';

import 'cubit/home_cubit.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();

  static BlocProvider<HomeCubit> provider() {
    return BlocProvider(
      create: (context) => HomeCubit(),
      child: HomeScreen(),
    );
  }
}

class _HomeScreenState extends CubitState<HomeScreen, HomeCubit, HomeState> {
  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
  }

  @override
  Widget builder(BuildContext context, HomeState state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: Column(
          children: [
            FlatButton(
              child: Text('Go to Login Stateful'),
              onPressed: () => navigator.pushNamed(AppRoute.loginScreen),
            ),
            FlatButton(
              child: Text('Go to Login Stateless'),
              onPressed: () =>
                  navigator.pushNamed(AppRoute.loginStatelessScreen),
            ),
          ],
        ),
      ),
    );
  }
}
