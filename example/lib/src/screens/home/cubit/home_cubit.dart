import 'package:simplest/simplest.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_state.dart';
part 'home_cubit.freezed.dart';

class HomeCubit extends GenericBaseCubit<HomeState> {
  HomeCubit() : super(HomeState.initial());
}
