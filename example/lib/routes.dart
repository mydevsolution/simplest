import 'package:example/src/screens/home/home_screen.dart';
import 'package:example/src/screens/login/login_stateless_screen.dart';
import 'package:flutter/material.dart';

import 'src/screens/login/login_screen.dart';
import 'src/screens/splash/splash_screen.dart';

class AppRoute {
  static const String splashScreen = '/splashScreen';
  static const String homeScreen = '/homeScreen';
  static const String loginScreen = '/loginScreen';
  static const String loginStatelessScreen = '/loginStatelessScreen';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final mapData =
        settings.arguments as Map<String, dynamic> ?? Map<String, dynamic>();
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (context) => SplashScreen.provider(), settings: settings);
      case splashScreen:
        return MaterialPageRoute(
            builder: (context) => SplashScreen.provider(), settings: settings);
      case loginScreen:
        return MaterialPageRoute(
            builder: (context) => LoginScreen.provider(), settings: settings);
      case loginStatelessScreen:
        return MaterialPageRoute(
            builder: (context) => LoginStatelessScreen.provider(),
            settings: settings);
      case homeScreen:
        return MaterialPageRoute(
            builder: (context) => HomeScreen.provider(), settings: settings);
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
